# Terraform deployment for wild-beavers giltab

Contains projects, project defaults options, labels, etc.

## Usage

This repository uses gitlab-ci. It will automatically deploy changes when pushed to master. In addition, it will execute plans on feature branches and on merge requests.

**DO NOT RUN `terraform` LOCALLY !**


<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.3.0 |
| <a name="requirement_gitlab"></a> [gitlab](#requirement\_gitlab) | ~> 3.6 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_gitlab"></a> [gitlab](#provider\_gitlab) | ~> 3.6 |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_group"></a> [group](#module\_group) | ./modules/group | n/a |
| <a name="module_project"></a> [project](#module\_project) | ./modules/project | n/a |

## Resources

| Name | Type |
|------|------|
| [gitlab_group.wild_beavers](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/data-sources/group) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_gitlab_token"></a> [gitlab\_token](#input\_gitlab\_token) | gitlab application token. [See how to create a personal access token on gitlab](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html). | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_projects"></a> [projects](#output\_projects) | n/a |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
