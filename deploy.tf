locals {
  tags = []
}

module "group" {
  source = "./modules/group"

  for_each = local.groups

  auto_devops_enabled               = lookup(each.value, "auto_devops_enabled", false)
  default_branch_protection         = lookup(each.value, "default_branch_protection", 3)
  description                       = lookup(each.value, "description")
  emails_disabled                   = lookup(each.value, "emails_disabled", false)
  group_memberships                 = lookup(each.value, "group_memberships", {})
  lfs_enabled                       = lookup(each.value, "lfs_enabled", false)
  mentions_disabled                 = lookup(each.value, "mentions_disabled ", false)
  name                              = each.key
  parent_id                         = lookup(each.value, "parent_id", 0)
  path                              = lookup(each.value, "path")
  project_creation_level            = lookup(each.value, "project_creation_level", "maintainer")
  request_access_enabled            = lookup(each.value, "request_access_enabled", false)
  require_two_factor_authentication = lookup(each.value, "require_two_factor_authentication", true)
  subgroup_creation_level           = lookup(each.value, "subgroup_creation_level", "owner")
  two_factor_grace_period           = lookup(each.value, "two_factor_grace_period", 336)
  visibility_level                  = lookup(each.value, "visibility_level", "public")
}

module "project" {
  source = "./modules/project"

  for_each = merge(
    local.terraform_projects,
    local.docker_projects,
    local.pipelines_projects,
    local.templates_projects
  )

  group_id = lookup(each.value, "group_id")

  name = each.key

  description                           = lookup(each.value, "description")
  analytics_access_level                = lookup(each.value, "analytics_access_level", "private")
  import_url                            = lookup(each.value, "import_url", null)
  tags                                  = lookup(each.value, "tags", local.tags)
  default_branch_push_access_level      = lookup(each.value, "default_branch_push_access_level", "no one")
  container_registry_enabled            = lookup(each.value, "container_registry_enabled", false)
  pages_access_level                    = lookup(each.value, "pages_access_level", "disabled")
  only_allow_merge_if_pipeline_succeeds = lookup(each.value, "only_allow_merge_if_pipeline_succeeds", true)
}
