0.5.1
=====

* fix: enable container registry for kind image

0.5.0
=====

* feat: adds terraform `module-aws-cloudfront`

0.4.0
=====

* feat: update gitlab groups/projects with new features
* feat: forces OTP to access groups
* feat: forces `only_allow_merge_if_pipeline_succeeds` to `true`
* maintenance: updates Terraform code to comply with tf 1.3+
* maintenance: pre-commit dependencies updates

0.3.0
=====

* feat: (Terraform) add repository `module-nexus-user`
* chore: use `https` instead of `git` protocol for pre-commit hooks
* chore: bump pre-commit hooks

0.2.0
=====

* feat: add docker jenkins master repo

0.1.0
=====

* feat: add new repos concerning ECS

0.0.1
=====

* feat: add variable to enable or not container registry on repo

0.0.0
=====

* tech: initialise repository
