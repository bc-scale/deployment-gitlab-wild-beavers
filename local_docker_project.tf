locals {
  docker_projects = {
    aws-cli : {
      description                = "AWS-CLI container image build files."
      tags                       = concat(local.docker_tags, ["aws-cli"]),
      group_id                   = module.group["docker"].id
      container_registry_enabled = true
    }
    aws-efs-mount : {
      description                = "AWS EFS mount command container image build files."
      tags                       = concat(local.docker_tags, ["aws-efs"]),
      group_id                   = module.group["docker"].id
      container_registry_enabled = true
    }
    kind : {
      description                = "KinD container image build files."
      tags                       = concat(local.docker_tags, ["kind"]),
      group_id                   = module.group["docker"].id
      container_registry_enabled = true
    }
    lighthouse-exporter : {
      description                = "Container used to run prometheus lighthouse-exporter."
      tags                       = concat(local.docker_tags, ["lighthouse-exporter", "prometheus", "exporter"]),
      group_id                   = module.group["docker"].id
      container_registry_enabled = true
    }
    pre-commit : {
      description                = "pre-commit container image build files."
      tags                       = concat(local.docker_tags, ["pre-commit"]),
      group_id                   = module.group["docker"].id
      container_registry_enabled = true
    }
    jenkins-master : {
      description                = "Jenkins master container image with docker cli."
      tags                       = concat(local.docker_tags, ["jenkins"]),
      group_id                   = module.group["docker"].id
      container_registry_enabled = true
    }
  }
  docker_tags = ["docker", "container"]
}
