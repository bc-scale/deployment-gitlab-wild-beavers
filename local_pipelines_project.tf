locals {
  pipelines_projects = {
    gitlab-ci : {
      description        = "Library of gitlab-ci pipelines and stages"
      tags               = concat(local.pipelines_tags, ["gitlab-ci"]),
      group_id           = module.group["pipelines"].id
      pages_access_level = "enabled"
    }
  }
  pipelines_tags = ["pipelines"]
}
