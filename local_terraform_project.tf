locals {
  terraform_projects = {
    deployment-gitlab-wild-beavers : {
      description                      = "Deployment project to setup gitlab repositories, settings and groups."
      tags                             = concat(local.terraform_deployment_tags, ["gitlab"]),
      default_branch_push_access_level = "maintainer"
      group_id                         = module.group["terraform"].id
    }

    # AWS
    module-aws-backup : {
      description = "Terraform module to use AWS Backup."
      import_url  = "https://scm.dazzlingwrench.fxinnovation.com/fxinnovation-public/terraform-module-aws-backup.git"
      tags        = concat(local.terraform_module_tags, ["aws", "backup"])
      group_id    = module.group["terraform"].id
    }
    module-aws-batch : {
      description = "Terraform module to create an AWS Batch compute environment."
      import_url  = "https://scm.dazzlingwrench.fxinnovation.com/fxinnovation-public/terraform-module-aws-batch.git"
      tags        = concat(local.terraform_module_tags, ["aws", "batch"])
      group_id    = module.group["terraform"].id
    }
    module-aws-batch-job : {
      description = "Terraform module to create an AWS Batch job."
      import_url  = "https://scm.dazzlingwrench.fxinnovation.com/fxinnovation-public/terraform-module-aws-batch-job.git"
      tags        = concat(local.terraform_module_tags, ["aws", "batch"])
      group_id    = module.group["terraform"].id
    }
    module-aws-bootstrap : {
      description = "Terraform module to create S3 bucket for Terraform state files."
      import_url  = "https://scm.dazzlingwrench.fxinnovation.com/fxinnovation-public/terraform-module-aws-bootstrap-s3.git"
      tags        = concat(local.terraform_module_tags, ["aws", "s3"])
      group_id    = module.group["terraform"].id
    }
    module-aws-bucket-s3 : {
      description = "Terraform module to create S3 Buckets."
      import_url  = "https://scm.dazzlingwrench.fxinnovation.com/fxinnovation-public/terraform-module-aws-bucket-s3.git"
      tags        = concat(local.terraform_module_tags, ["aws", "s3"])
      group_id    = module.group["terraform"].id
    }
    module-aws-codebuild : {
      description = "Terraform module to CodeBuild projects."
      import_url  = "https://github.com/cloudposse/terraform-aws-codebuild.git"
      tags        = concat(local.terraform_module_tags, ["aws", "codebuild"])
      group_id    = module.group["terraform"].id
    }
    module-aws-cloudwatch-log-group : {
      description = "Terraform module to create an CloudWatch Log Group."
      import_url  = "https://scm.dazzlingwrench.fxinnovation.com/fxinnovation-public/terraform-module-aws-cloudwatch-log-group.git"
      tags        = concat(local.terraform_module_tags, ["aws", "cloudwatch"])
      group_id    = module.group["terraform"].id
    }
    module-aws-cost-optimization : {
      description = "Terraform module to enable cost optimizations on AWS."
      import_url  = "https://scm.dazzlingwrench.fxinnovation.com/fxinnovation-public/terraform-module-aws-cost-optimization.git"
      tags        = concat(local.terraform_module_tags, ["aws", "cost-optimizations"])
      group_id    = module.group["terraform"].id
    }
    module-aws-efs : {
      description = "Terraform module to create an EFS."
      import_url  = "https://scm.dazzlingwrench.fxinnovation.com/fxinnovation-public/terraform-module-aws-efs.git"
      tags        = concat(local.terraform_module_tags, ["aws", "efs"])
      group_id    = module.group["terraform"].id
    }
    module-aws-eks : {
      description = "Terraform module to create an EKS cluster."
      import_url  = "https://scm.dazzlingwrench.fxinnovation.com/fxinnovation-public/terraform-module-aws-eks.git"
      tags        = concat(local.terraform_module_tags, ["aws", "eks"])
      group_id    = module.group["terraform"].id
    }
    module-aws-iam-group : {
      description = "Terraform module to create an IAM group with associated role."
      import_url  = "https://gitlab.com/wild-beavers/templates/templates-terraform.git"
      tags        = concat(local.terraform_module_tags, ["aws", "iam"])
      group_id    = module.group["terraform"].id
    }
    module-aws-eks-service-account-role : {
      description = "Terraform module to create an EKS Service Account linked to an IAM Role."
      import_url  = "https://scm.dazzlingwrench.fxinnovation.com/fxinnovation-public/terraform-module-aws-eks-service-account-role.git"
      tags        = concat(local.terraform_module_tags, ["aws", "eks", "iam"])
      group_id    = module.group["terraform"].id
    }
    module-aws-eks-worker-pool : {
      description = "Terraform module to create an EKS worker pool."
      import_url  = "https://scm.dazzlingwrench.fxinnovation.com/fxinnovation-public/terraform-module-aws-eks-worker-pool.git"
      tags        = concat(local.terraform_module_tags, ["aws", "eks", "asg"])
      group_id    = module.group["terraform"].id
    }
    module-aws-fargate : {
      description = "Terraform module to use fargate."
      import_url  = "https://scm.dazzlingwrench.fxinnovation.com/fxinnovation-public/terraform-module-aws-fargate.git"
      tags        = concat(local.terraform_module_tags, ["aws", "fargate"])
      group_id    = module.group["terraform"].id
    }
    module-aws-organization : {
      description = "Terraform module to manage an AWS Organization."
      import_url  = "https://scm.dazzlingwrench.fxinnovation.com/fxinnovation-public/terraform-module-aws-organization.git"
      tags        = concat(local.terraform_module_tags, ["aws", "organization"])
      group_id    = module.group["terraform"].id
    }
    module-aws-route53 : {
      description = "Terraform module to use AWS Route53."
      import_url  = "https://scm.dazzlingwrench.fxinnovation.com/fxinnovation-public/terraform-module-aws-route53.git"
      tags        = concat(local.terraform_module_tags, ["aws", "route53"])
      group_id    = module.group["terraform"].id
    }
    module-aws-secret-manager : {
      description = "Terraform module to create Secret Manager resources."
      tags        = concat(local.terraform_module_tags, ["aws", "secret_manager"])
      group_id    = module.group["terraform"].id
    }
    module-aws-service-catalog-portfolio : {
      description = "Terraform module to create a service-catalog portfolio."
      tags        = concat(local.terraform_module_tags, ["aws", "service-catalog"])
      group_id    = module.group["terraform"].id
    }
    module-aws-service-catalog-product : {
      description = "Terraform module to create a service-catalog product."
      tags        = concat(local.terraform_module_tags, ["aws", "service-catalog"])
      group_id    = module.group["terraform"].id
    }
    module-aws-service-user : {
      description = "Terraform module to create a service user."
      import_url  = "https://scm.dazzlingwrench.fxinnovation.com/fxinnovation-public/terraform-module-aws-service-user.git"
      tags        = concat(local.terraform_module_tags, ["aws"])
      group_id    = module.group["terraform"].id
    }
    module-aws-sg-rules-active-directory : {
      description = "Terraform module to create an SG rules for Microsoft Active Directory."
      import_url  = "https://scm.dazzlingwrench.fxinnovation.com/fxinnovation-public/terraform-module-aws-sg-rules-active-directory.git"
      tags        = concat(local.terraform_module_tags, ["aws", "sg"])
      group_id    = module.group["terraform"].id
    }
    module-aws-ssm-documents : {
      description = "Terraform module to create SSM documents."
      import_url  = "https://scm.dazzlingwrench.fxinnovation.com/fxinnovation-public/terraform-module-aws-ssm-documents.git"
      tags        = concat(local.terraform_module_tags, ["aws", "ssm"])
      group_id    = module.group["terraform"].id
    }
    module-aws-ssm-parameters : {
      description = "Terraform module to create SSM parameters."
      import_url  = "https://scm.dazzlingwrench.fxinnovation.com/fxinnovation-public/terraform-module-aws-ssm-parameters.git"
      tags        = concat(local.terraform_module_tags, ["aws", "ssm"])
      group_id    = module.group["terraform"].id
    }
    module-aws-transit-gateway : {
      description = "Terraform module to create a transit gateway."
      import_url  = "https://scm.dazzlingwrench.fxinnovation.com/fxinnovation-public/terraform-module-aws-transit-gateway.git"
      tags        = concat(local.terraform_module_tags, ["aws", "transit-gateway"])
      group_id    = module.group["terraform"].id
    }
    module-aws-terraform-baseline : {
      description = "Terraform module to create associated resources helpful for Terraform."
      import_url  = "https://gitlab.com/wild-beavers/templates/templates-terraform.git"
      tags        = concat(local.terraform_module_tags, ["aws", "iam"])
      group_id    = module.group["terraform"].id
    }
    module-aws-virtual-machine : {
      description = "Terraform module to create a virtual machine."
      import_url  = "https://scm.dazzlingwrench.fxinnovation.com/fxinnovation-public/terraform-module-aws-virtual-machine.git"
      tags        = concat(local.terraform_module_tags, ["aws", "ec2"])
      group_id    = module.group["terraform"].id
    }
    module-aws-ecs : {
      description = "Terraform module to create ECS resources."
      tags        = concat(local.terraform_module_tags, ["aws", "ecs"])
      group_id    = module.group["terraform"].id
    }
    module-aws-cloudfront : {
      description = "Terraform module to create Cloudfront resources."
      tags        = concat(local.terraform_module_tags, ["aws", "cloudfront"])
      group_id    = module.group["terraform"].id
    }

    # Bitbucket
    module-bitbucket-repository : {
      description = "Terraform module to create bitbucket repositories with branch protection."
      tags        = concat(local.terraform_module_tags, ["bitbucket", "repository"])
      group_id    = module.group["terraform"].id
    }

    # Kubernetes
    module-kubernetes-anchore : {
      description = "Terraform module to deploy anchore on kubernetes."
      tags        = concat(local.terraform_module_tags, ["kubernetes", "aws-health-exporter"])
      import_url  = "https://scm.dazzlingwrench.fxinnovation.com/fxinnovation-public/terraform-module-kubernetes-anchore.git"
      group_id    = module.group["terraform"].id
    }
    module-kubernetes-aws-health-exporter : {
      description = "Terraform module to deploy aws-health-exporter on kubernetes."
      tags        = concat(local.terraform_module_tags, ["kubernetes", "aws-health-exporter"])
      import_url  = "https://scm.dazzlingwrench.fxinnovation.com/fxinnovation-public/terraform-module-kubernetes-aws-health-exporter.git"
      group_id    = module.group["terraform"].id
    }
    module-kubernetes-azure-metrics-exporter : {
      description = "Terraform module to deploy azure-metrics-exporter on kubernetes."
      tags        = concat(local.terraform_module_tags, ["kubernetes", "azure-metrics-exporter"])
      import_url  = "https://scm.dazzlingwrench.fxinnovation.com/fxinnovation-public/terraform-module-kubernetes-azure-metrics-exporter.git"
      group_id    = module.group["terraform"].id
    }
    module-kubernetes-blackbox-exporter : {
      description = "Terraform module to deploy blackbox-exporter on kubernetes."
      tags        = concat(local.terraform_module_tags, ["kubernetes", "blackbox-exporter"])
      import_url  = "https://scm.dazzlingwrench.fxinnovation.com/fxinnovation-public/terraform-module-kubernetes-blackbox-exporter.git"
      group_id    = module.group["terraform"].id
    }
    module-kubernetes-cloudwatch-agent : {
      description = "Terraform module to deploy cloudwatch-agent on kubernetes."
      tags        = concat(local.terraform_module_tags, ["kubernetes", "cloudwatch-agent"])
      import_url  = "https://scm.dazzlingwrench.fxinnovation.com/fxinnovation-public/terraform-module-kubernetes-cloudwatch-agent.git"
      group_id    = module.group["terraform"].id
    }
    module-kubernetes-cloudwatch-agent-prometheus : {
      description = "Terraform module to deploy the prometheus version of cloudwatch-agent on kubernetes."
      tags        = concat(local.terraform_module_tags, ["kubernetes", "cloudwatch-agent", "prometheus"])
      import_url  = "https://scm.dazzlingwrench.fxinnovation.com/fxinnovation-public/terraform-module-kubernetes-cloudwatch-agent-prometheus.git"
      group_id    = module.group["terraform"].id
    }
    module-kubernetes-cloudwatch-exporter : {
      description = "Terraform module to deploy cloudwatch-exporter on kubernetes."
      tags        = concat(local.terraform_module_tags, ["kubernetes", "cloudwatch-exporter"])
      import_url  = "https://scm.dazzlingwrench.fxinnovation.com/fxinnovation-public/terraform-module-kubernetes-cloudwatch-exporter.git"
      group_id    = module.group["terraform"].id
    }
    module-kubernetes-cluster-autoscaler : {
      description = "Terraform module to deploy cluster-autoscaler on kubernetes."
      tags        = concat(local.terraform_module_tags, ["kubernetes", "cluster-autoscaler"])
      import_url  = "https://scm.dazzlingwrench.fxinnovation.com/fxinnovation-public/terraform-module-kubernetes-cluster-autoscaler.git"
      group_id    = module.group["terraform"].id
    }
    module-kubernetes-grafana : {
      description = "Terraform module to deploy grafana on kubernetes."
      tags        = concat(local.terraform_module_tags, ["kubernetes", "grafana"])
      import_url  = "https://scm.dazzlingwrench.fxinnovation.com/fxinnovation-public/terraform-module-kubernetes-grafana.git"
      group_id    = module.group["terraform"].id
    }
    module-kubernetes-jenkins : {
      description = "Terraform module to deploy jenkins on kubernetes."
      tags        = concat(local.terraform_module_tags, ["kubernetes", "jenkins"])
      import_url  = "https://scm.dazzlingwrench.fxinnovation.com/fxinnovation-public/terraform-module-kubernetes-jenkins.git"
      group_id    = module.group["terraform"].id
    }
    module-kubernetes-jmeter : {
      description = "Terraform module to deploy jmeter on kubernetes."
      tags        = concat(local.terraform_module_tags, ["kubernetes", "jmeter"])
      import_url  = "https://scm.dazzlingwrench.fxinnovation.com/fxinnovation-public/terraform-module-kubernetes-jmeter.git"
      group_id    = module.group["terraform"].id
    }
    module-kubernetes-keycloak-gatekeeper : {
      description = "Terraform module to deploy keycloak-gatekeeper on kubernetes."
      tags        = concat(local.terraform_module_tags, ["kubernetes", "keycloak-gatekeeper"])
      import_url  = "https://scm.dazzlingwrench.fxinnovation.com/fxinnovation-public/terraform-module-kubernetes-keycloak-gatekeeper.git"
      group_id    = module.group["terraform"].id
    }
    module-kubernetes-kube-state-metrics : {
      description = "Terraform module to deploy kube-state-metrics on kubernetes."
      tags        = concat(local.terraform_module_tags, ["kubernetes", "kube-state-metrics"])
      import_url  = "https://scm.dazzlingwrench.fxinnovation.com/fxinnovation-public/terraform-module-kubernetes-kube-state-metrics.git"
      group_id    = module.group["terraform"].id
    }
    module-kubernetes-lighthouse-exporter : {
      description = "Terraform module to deploy lighthouse-exporter on kubernetes."
      tags        = concat(local.terraform_module_tags, ["kubernetes", "lighthouse-exporter", "prometheus"])
      group_id    = module.group["terraform"].id
    }
    module-kubernetes-nginx-ingress-controller : {
      description = "Terraform module to deploy nginx-ingress-controller on kubernetes."
      tags        = concat(local.terraform_module_tags, ["kubernetes", "nginx-ingress-controller"])
      import_url  = "https://scm.dazzlingwrench.fxinnovation.com/fxinnovation-public/terraform-module-kubernetes-nginx-ingress-controller.git"
      group_id    = module.group["terraform"].id
    }
    module-kubernetes-node-exporter : {
      description = "Terraform module to deploy node-exporter on kubernetes."
      tags        = concat(local.terraform_module_tags, ["kubernetes", "node-exporter"])
      import_url  = "https://scm.dazzlingwrench.fxinnovation.com/fxinnovation-public/terraform-module-kubernetes-node-exporter.git"
      group_id    = module.group["terraform"].id
    }
    module-kubernetes-nexus3 : {
      description = "Terraform module to deploy nexus3 on kubernetes."
      tags        = concat(local.terraform_module_tags, ["kubernetes", "nexus3"])
      import_url  = "https://scm.dazzlingwrench.fxinnovation.com/fxinnovation-public/terraform-module-kubernetes-nexus3.git"
      group_id    = module.group["terraform"].id
    }
    module-kubernetes-nvidia-device-plugin : {
      description = "Terraform module to deploy nvidia-device-plugin on kubernetes."
      tags        = concat(local.terraform_module_tags, ["kubernetes", "nvidia"])
      import_url  = "https://scm.dazzlingwrench.fxinnovation.com/fxinnovation-public/terraform-module-kubernetes-nvidia-device-plugin.git"
      group_id    = module.group["terraform"].id
    }
    module-kubernetes-oracledb-exporter : {
      description = "Terraform module to deploy oracledb-exporter on kubernetes."
      tags        = concat(local.terraform_module_tags, ["kubernetes", "oracledb-exporter"])
      import_url  = "https://scm.dazzlingwrench.fxinnovation.com/fxinnovation-public/terraform-module-kubernetes-oracledb-exporter.git"
      group_id    = module.group["terraform"].id
    }
    module-kubernetes-prometheus : {
      description = "Terraform module to deploy Prometheus on kubernetes."
      tags        = concat(local.terraform_module_tags, ["kubernetes", "prometheus"])
      group_id    = module.group["terraform"].id
    }
    module-kubernetes-redis : {
      description = "Terraform module to deploy redis on kubernetes."
      tags        = concat(local.terraform_module_tags, ["kubernetes", "redis"])
      import_url  = "https://scm.dazzlingwrench.fxinnovation.com/fxinnovation-public/terraform-module-kubernetes-redis.git"
      group_id    = module.group["terraform"].id
    }
    module-kubernetes-redis-exporter : {
      description = "Terraform module to deploy redis-exporter on kubernetes."
      tags        = concat(local.terraform_module_tags, ["kubernetes", "redis-exporter"])
      import_url  = "https://scm.dazzlingwrench.fxinnovation.com/fxinnovation-public/terraform-module-kubernetes-redis-exporter.git"
      group_id    = module.group["terraform"].id
    }
    module-kubernetes-renovate : {
      description = "Terraform module to deploy renovate on kubernetes."
      tags        = concat(local.terraform_module_tags, ["kubernetes", "renovate"])
      group_id    = module.group["terraform"].id
    }
    module-kubernetes-runscope-radar : {
      description = "Terraform module to deploy runscope-radar on kubernetes."
      tags        = concat(local.terraform_module_tags, ["kubernetes", "runscope-radar"])
      import_url  = "https://scm.dazzlingwrench.fxinnovation.com/fxinnovation-public/terraform-module-kubernetes-runscope-radar.git"
      group_id    = module.group["terraform"].id
    }

    # Nexus
    module-nexus-user : {
      description = "Terraform module to deploy Nexus users."
      tags        = concat(local.terraform_module_tags, ["nexus"])
      group_id    = module.group["terraform"].id
    }
  }

  terraform_tags            = ["terraform"]
  terraform_deployment_tags = concat(local.terraform_tags, ["deployment"])
  terraform_module_tags     = concat(local.terraform_tags, ["module"])
}
