variable "name" {
  type = string
}

variable "path" {
  type = string
}

variable "description" {
  type = string
}

variable "lfs_enabled" {
  type    = bool
  default = false
}

variable "default_branch_protection" {
  type = number
}

variable "request_access_enabled" {
  type    = bool
  default = false
}

variable "visibility_level" {
  type    = string
  default = "public"
}

variable "share_with_group_lock" {
  type    = bool
  default = true
}

variable "project_creation_level" {
  type    = string
  default = "noone"
}

variable "auto_devops_enabled" {
  type    = bool
  default = false
}

variable "emails_disabled" {
  type    = bool
  default = false
}

variable "mentions_disabled" {
  type    = bool
  default = false
}

variable "subgroup_creation_level" {
  type    = string
  default = "owner"
}

variable "require_two_factor_authentication" {
  type    = bool
  default = false
}

variable "two_factor_grace_period" {
  type    = number
  default = null
}

variable "parent_id" {
  type    = number
  default = null
}

variable "group_memberships" {
  type = map(object({
    user_id      = number
    access_level = optional(string)
    expires_at   = optional(string)
  }))
}
