#####
# Projects
#####

locals {
  default_branch = "master"

  default_container_registry_enabled = var.container_registry_enabled
  default_issues_enabled             = true
  default_lfs_enabled                = false
  default_merge_requests_enabled     = true
  default_packages_enabled           = false
  default_pipelines_enabled          = true
  default_request_access_enabled     = true
  default_shared_runners_enabled     = false
  default_snippets_enabled           = false
  default_wiki_enabled               = false

  default_merge_method                                     = "ff"
  default_only_allow_merge_if_all_discussions_are_resolved = true
  default_resolve_outdated_diff_discussions                = true
  default_only_allow_merge_if_pipeline_succeeds            = true
  default_security_and_compliance_access_level             = "private"
  default_approvals_before_merge                           = 2

  // All commit messages must match this regex
  commit_message_regex = "(feat|fix|chore|tech|refactor|doc|maintenance|test|revert): "
  // All commited filenames must not match this regex
  file_name_regex = "(exe|tar|tar.gz|tar.xz|zip|7zip|rar|bin|jar|docx|xlsx|xls)$"
  // Users can only push commits to this repository that were committed with one of their own verified emails.
  commit_committer_check = true
  deny_delete_tag        = true
  // GitLab will reject any files that are likely to contain secrets.
  prevent_secrets         = true
  reject_unsigned_commits = true
  // Maximum file size (MB).
  max_file_size = 2

  default_labels = {
    "critical bug::critical" : {
      description = "Bug that impairs the main features of the tool; Must be fixes as soon as possible"
      color       = "#FF0000"
    },
    "bug::high" : {
      description = "Generic bug in the main features of the tool, with a known/standard way to fix it"
      color       = "#FF0000"
    },
    "edge bug::medium" : {
      description = "Bug on a side; not frequently used feature"
      color       = "#c39953"
    },
    "heisenbug::medium" : {
      description = "Bug that seems to disapear in an attempt to study it"
      color       = "#c39953"
    },
    "chore" : {
      description = "Issue that requires some basic maintenance of the code: pins new version, dependency updates, etc"
      color       = "#5843AD"
    },
    "doc" : {
      description = "Issue that requires editing or adding new documentation"
      color       = "#5843AD"
    },
    "enhancement" : {
      description = "Issue that ask for a new feature"
      color       = "#5843AD"
    },
    "need more info" : {
      description = "Issue that require the input of a user before taking any action"
      color       = "#6699cc"
    },
    "to describe" : {
      description = "Issue not sufficiently described. All the necessary information is known, but the issue itself lacks written information."
      color       = "#6699cc"
    },
    "to sort" : {
      description = "Issue that need some labeling."
      color       = "#6699cc"
    }
  }
}

resource "gitlab_project" "this" {
  name        = var.name
  description = var.description
  tags        = var.tags

  namespace_id                     = var.group_id
  default_branch                   = local.default_branch
  visibility_level                 = "public"
  initialize_with_readme           = true
  remove_source_branch_after_merge = true
  allow_merge_on_skipped_pipeline  = false

  analytics_access_level     = var.analytics_access_level
  container_registry_enabled = local.default_container_registry_enabled
  issues_enabled             = local.default_issues_enabled
  lfs_enabled                = local.default_lfs_enabled
  merge_requests_enabled     = local.default_merge_requests_enabled
  packages_enabled           = local.default_packages_enabled
  pages_access_level         = var.pages_access_level
  pipelines_enabled          = local.default_pipelines_enabled
  request_access_enabled     = local.default_request_access_enabled
  shared_runners_enabled     = local.default_shared_runners_enabled
  snippets_enabled           = local.default_snippets_enabled
  wiki_enabled               = local.default_wiki_enabled

  merge_method                                     = local.default_merge_method
  only_allow_merge_if_pipeline_succeeds            = coalesce(var.only_allow_merge_if_pipeline_succeeds, local.default_only_allow_merge_if_pipeline_succeeds)
  approvals_before_merge                           = local.default_approvals_before_merge
  only_allow_merge_if_all_discussions_are_resolved = local.default_only_allow_merge_if_all_discussions_are_resolved
  security_and_compliance_access_level             = local.default_security_and_compliance_access_level
  resolve_outdated_diff_discussions                = local.default_resolve_outdated_diff_discussions

  push_rules {
    commit_message_regex    = local.commit_message_regex
    file_name_regex         = local.file_name_regex
    commit_committer_check  = local.commit_committer_check
    deny_delete_tag         = local.deny_delete_tag
    prevent_secrets         = local.prevent_secrets
    reject_unsigned_commits = local.reject_unsigned_commits
    max_file_size           = local.max_file_size
  }

  import_url = var.import_url

  lifecycle {
    prevent_destroy = true
  }
}

resource "gitlab_project_level_mr_approvals" "this" {
  project_id = gitlab_project.this.id

  disable_overriding_approvers_per_merge_request = true
  merge_requests_author_approval                 = true
  merge_requests_disable_committers_approval     = false
  require_password_to_approve                    = false
  reset_approvals_on_push                        = false
}

resource "gitlab_label" "this" {
  for_each = local.default_labels

  project = gitlab_project.this.id

  name        = each.key
  description = lookup(each.value, "description")
  color       = lookup(each.value, "color")
}

resource "gitlab_branch_protection" "this" {
  project = gitlab_project.this.id

  branch             = local.default_branch
  push_access_level  = var.default_branch_push_access_level
  merge_access_level = "maintainer"
}

resource "gitlab_project_badge" "this" {
  project   = gitlab_project.this.id
  link_url  = "https://gitlab.com/%%{project_path}/-/commits/%%{default_branch}"
  image_url = "https://gitlab.com/%%{project_path}/badges/%%{default_branch}/pipeline.svg"
}
