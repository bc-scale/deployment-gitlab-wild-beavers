#####
# Groups
#####

variable "group_id" {
  type = string
}

#####
# Project
#####

variable "name" {
  type = string

  validation {
    condition     = can(regex("^[a-z0-9-]+$", var.name))
    error_message = "The var.project_name must match “^[a-z0-9-]+$”."
  }
}

variable "tags" {
  type    = list(string)
  default = []
}

variable "description" {
  type = string

  validation {
    condition     = 8 <= length(var.description)
    error_message = "The var.project_description length must be greater than 8 characters."
  }
}

variable "analytics_access_level" {
  type = string

  validation {
    condition     = contains(["disabled", "private", "enabled"], var.analytics_access_level)
    error_message = "The var.analytics_access_level must be “disabled”, “private” or “enabled”."
  }
}

variable "only_allow_merge_if_pipeline_succeeds" {
  type    = bool
  default = null
}

variable "default_branch_push_access_level" {
  type = string

  validation {
    condition     = contains(["no one", "guest", "reporter", "developer", "maintainer", "owner", "master"], var.default_branch_push_access_level)
    error_message = "The var.default_branch_push_access_level must be “no one”, “guest”, “reporter”, “developer”, “maintainer”, “owner” or “master”."
  }
}

variable "pages_access_level" {
  type    = string
  default = "disabled"

  validation {
    condition     = contains(["disabled", "private", "enabled", "public"], var.pages_access_level)
    error_message = "The var.pages_access_level must be “disabled”, “private”, “enabled”, “public” or “private”."
  }
}

variable "import_url" {
  type    = string
  default = null
}

variable "container_registry_enabled" {
  type    = bool
  default = false
}
